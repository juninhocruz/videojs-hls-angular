import {Component} from '@angular/core';

declare const require: any;
declare const window: any;

const videojs = require('./lib/videojs-hls/video.js');
window.videojs = videojs;
require('./lib/videojs-hls/videojs-contrib-hls.js');

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  player;

  prepare(video) {
    const options = {};

    this.player = videojs(video, options, function onPlayerReady() {
      console.log('Your player is ready!');


      // How about an event listener?
      this.on('ended', function() {
        console.log('Awww...over so soon?!');
      });
    });
    video.poster = 'http://10.42.0.1/videos/mp4/thumb.png';
    this.player.src({
      src: 'http://10.42.0.1/videos/live3/live3.m3u8',
      type: 'application/x-mpegURL',
      withCredentials: false
    });
  }

  play() {
    this.player.play();
  }
}
